/*          Análise Númeria turma 2017.2                                     */
/*          Discente: José Carlos da Silva Adão  nº matricula 201020048      */
/*                                                                           */

/*----------------------Implementação Método Fatoração LU--------------------*/
/*---------------------------Definição da Matriz-----------------------------*/
A = [ 6 2 -1;2 4 1;3 2 8]; //matriz de entrada Exemplo 4.3 pg 122
B = [7; 7; 13;];  //solução do sistema
TAM = length ( B ) ;//tamanho
/*---------------------------------------------------------------------------*/


/*---------------------------Processamento do Metodo-------------------------*/
if  (det(A)==0) then
    disp("O determinante é 0 portanto o sistema não tem solução")
else
    for i = 1 : TAM
        L(i,i) = 1
        for j = i + 1 : TAM
            L(i,j) = 0
        end
    end
    
    /* Calculando a Linha i de U*/
    for i = 1 : TAM
        for j = i : TAM                                  
            soma = 0
            for aux = 1 : i-1
                soma = soma + L(i,aux)*U(aux,j)
            end
             U(i,j) = A(i,j) - soma
        end
    /*Calculando a Coluna j de L*/
        for j = i+1 : TAM                                
            soma = 0
            for aux = 1 : i-1
                soma = soma + L(j,aux) * U(aux,i)
            end
        L(j,i) = ( A(j,i) - soma ) / U(i,i); 
        end
    end

/*---------------------------Solução-----------------------------*/
Y(1) = B(1)
 for aux = 2 : TAM
    soma =0
    for j = 1 : aux -1
       soma = soma + L(aux,j) * Y(j);
       Y(aux) = B(aux) - soma       
    end       
 end
 
 X(TAM) = Y(TAM)/U(TAM,TAM)
 
 aux = TAM-1
 while(aux>0)
     soma =0
     for j = aux +1 : TAM
         soma = soma + U(aux,j) * X(j)             
     end
     X(aux) = (Y(aux) - soma)/U(aux,aux)
     aux = aux -1
 end
/*--------------------------------------Saida na tela-------------------------------*/    
disp ("Matriz de Entrada")
disp (A)
disp("Matriz L ")
disp (L)
disp ("Matriz U ")
disp (U)
disp ("Vetor Y ")
disp (Y)
disp ("Vetor Solução")
disp (X)
end
/*----------------------------------------------------------------------------------*/
