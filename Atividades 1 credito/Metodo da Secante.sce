/*          Análise Númeria turma 2017.2                                     */
/*          Discente: José Carlos da Silva Adão  nº matricula 201020048      */
/*                                                                           */

/*----------------------Implementação Método da Secante----------------------*/
/*-------------------------Declaração das funções----------------------------*/
function y = f(x);
    y = (1 / 48)*((693 * x^6) - (945 * x^4) + (315 * x^2) - 15);
endfunction
/*---------------------------------------------------------------------------*/
/*-----------------------------------Entradas--------------------------------*/
a = 0.01; //intervalo a
b = 0.1;  //intervalo b
erro = 1; //erro inicial
it = 1; //iteracão inicial
precisao = 10^(-2)
/*---------------------------------------------------------------------------*/

/*---------------------------Processamento do Metodo-------------------------*/
disp(" |Iter|  | Raiz|    | Erro |");
while(erro >= precisao)&it<200 do
    aux = a;
    a = b;
    secante = (f(a)-f(aux))/(a-aux)
    b = a - (f(a)/secante); //calcular o proximo b
    erro = abs((b-a)/ b);
        disp([it b erro]);
    it = it+1;
end
/*---------------------------------------------------------------------------*/
