/*          Análise Númeria turma 2017.2                                     */
/*          Discente: José Carlos da Silva Adão  nº matricula 201020048      */
/*                                                                           */

/*                      Implementação Método da Bissecção                    */



/*---------------------------Declaração das funções--------------------------*/
function [y] = funcao(x)

     y = (1 / 48)*((693 * x^6) - (945 * x^4) + (315 * x^2) - 15);// erro 10 ^-2 intervalo[0.1;1] Função 3.16 pg 106
//   y = 1 -(1+x+(x^2/2))*%e^-x;  //Função 3.3 pg 97
//   y =  x * %e^x -1;//erro 10^-2 [2;3] Funcão 3.30 pg 94

endfunction;
/*---------------------------------------------------------------------------*/


/*-----------------------------------Entradas--------------------------------*/
    a = 0.1; //intervalo inicial 0.1
    b = 1; //intervalo final 1
    ini = a;
    fim = b;
    interm = (a+b)/2;

    /*calcula a raiz de f(x) no intervalo [a,b] com precisão eps1*/
    x0=a;
    x1=b;
    xm=(x0+x1)./2;
    eps1 = 10^(-2); //Precisão
    it=1;           //número de iterações
/*---------------------------------------------------------------------------*/   

/*---------------------------Processamento do Metodo-------------------------*/
    
    if (funcao(x0)*funcao(x1)>=0) 
        printf("\nO valor de f(a) e f(b) devem ter sinal diferente");
        abort;
    end;
    
    while abs(funcao(xm)) > eps1&it<=500
        
        if funcao(x0)*funcao(xm) > 0 then 
            x0=xm; 
        else 
        x1=xm; 
        end;

        xm=(x0+x1)/2;
        it=it+1;
    end;
    
    raiz=xm;
    iter=it;

    if it>=499 then
        printf("\nNão converge nesse intervalo!");
        abort;
    else;
        printf("\nRaiz Bissecção é %10.15f com %d iterações", raiz, iter);
end;
/*---------------------------------------------------------------------------*/
