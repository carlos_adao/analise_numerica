/*          Análise Númeria turma 2017.2                                     */
/*          Discente: José Carlos da Silva Adão  nº matricula 201020048      */
/*                                                                           */

/*-------------------Implementação Método do Ponto Falso---------------------*/
/*-------- ----------------Declaração das funções----------------------------*/
function [y]=f(x)
    y = (1 / 48)*((693 * x^6) - (945 * x^4) + (315 * x^2) - 15);
endfunction
/*---------------------------------------------------------------------------*/
/*-----------------------------------Entradas--------------------------------*/
a = 0.1; //intervalo a
b = 1; //intervalo b
it = 1; //iteracão inicial
precisao = 10^(-2)//precisão
/*---------------------------------------------------------------------------*/
 
    
    b=f(a)
    e=0
    while(abs(b-a)>precisao&it<1000)
        if(abs(b-a)<100)
            e=e+1
            d=e-1
            a=b
            b=f(a)   
        else
            b=a
            printf('\n Não converge')
        end
        it = it+1;
    end
    e=e+1
    d=e-1
    z=abs(b-a)
    printf("\nRaiz Ponto Falso é %10.15f com %d iterações", z, it);
