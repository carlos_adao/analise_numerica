/*          Análise Númeria turma 2017.2                                     */
/*          Discente: José Carlos da Silva Adão  nº matricula 201020048      */
/*                                                                           */

/*----------------------Implementação Metodo de Jacobi-----------------------*/
/*---------------------------Definição da Matriz-----------------------------*/
A = [ 6 2 -1;2 4 1;3 2 8]; //matriz de entrada Exemplo 4.3 pg 122
b = [7; 7; 13;];  //solução do sistema
n = length ( B ) ;//tamanho
/*---------------------------------------------------------------------------*/

precisao = 10^(-2); //Precisão
x0 = [0.7; -1.6; 0.6]; //solução inicial
p = 10;
x = x0;
it = 0;
i= 0; s = 0;
/*---------------------------------------------------------------------------*/


/*---------------------------Processamento do Metodo-------------------------*/
//quantidade de linhas 
for i=1:n
//quantidade de colunas
    for s=1:n
        AA(i,s) = A(i,s)/A(i,i);
    end;
    be(i) = b(i)/A(i, i);
end;

AB = AA;
bc = be;

while p > precisao & it<=1000
    x = bc - ((AB-i)*x);
    p = norm((AB*x)-bc);
    disp (x) ;//saida tela
    it = it+1;
end;

if(it>999) then
    printf("Não converge nesse intervalo");
else
    printf("Total de iteracoes %f", it);
end;
/*---------------------------------------------------------------------------*/
