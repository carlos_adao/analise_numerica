/*          Análise Númeria turma 2017.2                                     */
/*          Discente: José Carlos da Silva Adão  nº matricula 201020048      */
/*                                                                           */

/*----------------------Implementação Gauss Seidel---------------------------*/
/*---------------------------Definição da Matriz-----------------------------*/
A = [ 5 1 1;3 4 1;3 3 6]; //matriz de entrada Exemplo 5.3 pg 166
B = [5; 6; 0;];  //solução do sistema
n = length ( B ) ;//tamanho
tol = 0.01;//tolerância
iter = 1;//inicio as iterações com 1
maxit = 5;// defino o maximo de iterações como sendo 5
/*---------------------------------------------------------------------------*/

x = zeros (n ,1) ; // inicializo a matriz
E = ones (n ,1) ; // inicializo a matriz 
S = diag ( diag ( A ) ) ;//incluo as diagonais 
T = S - A ;
xold = x ;

/*---------------------------Processamento do Metodo-------------------------*/
while (1)
    for i = 1: n
        x (i , iter +1) = ( B ( i ) + T (i ,:) * xold ) / A (i , i) ;
        E (i , iter +1) = ( x (i , iter +1) - xold ( i ) ) / x (i , iter+1) *100;
        xold ( i ) = x (i , iter +1) ;
    end

    if x (: , iter ) == 0
    E = 1;
    else
    E = sqrt (( sum(( E (: , iter +1) ) .^2) ) / n ) ;
    end

    if E <= tol | iter == maxit
    break
    end
    iter = iter + 1;
end
X = x (: , iter ) ;
x = round ( x *10^5) /10^5;
x (: ,1) = [];
/*---------------------------------------------------------------------------*/

/*---------------------------------------------Saida-------------------------*/
disp (x) ;
/*---------------------------------------------------------------------------*/
