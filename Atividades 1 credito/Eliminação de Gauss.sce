/*          Análise Númeria turma 2017.2                                     */
/*          Discente: José Carlos da Silva Adão  nº matricula 201020048      */
/*                                                                           */

/*-----------------Implementação Eliminação de Gauss Seidel------------------*/
/*---------------------------Definição da Matriz-----------------------------*/
A = [ 6 2 -1;2 4 1;3 2 8]; //matriz de entrada Exemplo 4.3 pg 122
B = [7; 7; 13;];  //solução do sistema
n = length ( B ) ;//tamanho
/*---------------------------------------------------------------------------*/

disp ("Matriz de Entrada")
disp (A)
disp ("Resultado Sistema")
disp (B)

calcula = [A , B ];

//Calculo a triangulação da matriz
for j = 1: n -1
//Particiono e encontro um pivo
    [ meio , t ] = max (abs( calcula ( j :n , j) ) ) ;
    linhasBaixo = t (1) +j -1;
    calcula ([ j , linhasBaixo ] ,:) = calcula ([ linhasBaixo , j ] ,:) ;
    
    for i = j +1: n
        calcula (i , j : n +1) = calcula (i ,j : n +1) - calcula (i , j ) / calcula(j , j ) * calcula (j , j : n +1) ;
    end
end
//Calculo os valores de x
x = zeros (n ,1);
x ( n ) = calcula (n , n +1) / calcula (n , n ) ;
for i = n -1: -1:1
 x ( i ) = ( calcula (i , n +1) - calcula (i , i +1: n ) * x ( i +1: n )) / calcula (i ,i );
end

disp ("Resultado de X")
disp ( x );
