/*          Análise Númeria turma 2017.2                                     */
/*          Discente: José Carlos da Silva Adão  nº matricula 201020048      */
/*                                                                           */

/*----------------------Implementação Método de Newton-----------------------*/
/*---------------------------Declaração das funções--------------------------*/
function y = f(x);
    y = (1 / 48)*((693 * x^6) - (945 * x^4) + (315 * x^2) - 15); //função
endfunction
function dy = df(x);
    dy= ((21 / 8) * (x)) * ((33 * x^(4)) - (30 * x^(2)) + 5) ;  //derivada
endfunction
/*---------------------------------------------------------------------------*/
/*-----------------------------------Entradas--------------------------------*/
a = 0.1; //intervalo a
b = 1; //intervalo b
it = 1; //iteracão inicial
precisao = 10^(-2)
/*---------------------------------------------------------------------------*/

/*---------------------------Processamento do Metodo-------------------------*/
disp("|Iter|  | Raiz|    | Erro |");
while(b >= precisao)&it<200 do
    aux = a; //guardar o valor inicial de a
    a = aux - (f(aux)/df(aux)); //calcular o proximo a
    b = abs((a-aux)/a);
    disp([it a b]);
    it = it+1;
end
/*---------------------------------------------------------------------------*/


