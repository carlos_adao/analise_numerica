
//                       Regra de Sipson 3/8                               //

//Aluno: Albert Paixão Santana

//ENTRADA

clc
//deff('y=f(x)','y=(%e^x) * cos(x)'); //intervalo a=0, b=1.2 NSub=6
deff('y=f(x)', 'y = x/(4+x)*%e^((-2*x)/10)');

a = 0;
b = 10;
NSub = 1;//Quantidade de subintervalos


//ALGORITMO

// Verifica o parâmetro do subintervalo
if(NSub == 0) then
    printf("Erro: Divisao por zero");
    return
end;

if(NSub < 0) then
    printf("Erro: Intervalo invalido");
    return
end;

h = (b-a)/NSub;

x = a;
soma = 0;

for(i = 1:NSub - 1)
    x = x + h;
    
    if(modulo(i, 3) == 0) then
        soma = soma + 2*f(x);
    else
        soma = soma + 3*f(x);
        end;
end;

soma = soma + f(a) + f(b);
I = (3 * h / 8) * soma;

printf("\nResultado da Integração: \n%f", I);

