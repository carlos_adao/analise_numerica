
//                  Aproximação Polinomial                        //

//Aluno: Albert Paixão Santana


//ENTRADA
clc

X = [1,2,3,4,5,6,7];
Y = [0.5,2.5,2.0,4.0,3.5,6.0,5.5];

GRAU = 3;

TAM = length(X);

//ALGORITMO

for i = 1: GRAU
   for j = 1 : TAM
       Mat(i,j) = X(j)^(i-1);
   end 
end


for i = 1 : GRAU
    for j =1 : GRAU
        Soma = 0;
        for aux = 1: TAM
            Soma =  Soma + Mat(i,aux) * Mat(j,aux);
        end
        
        A(i,j) = Soma;
    end
end

for i = 1 : GRAU
    Soma = 0;
    for j = 1: TAM
        Soma = Soma + Y(j)*Mat(i,j);
    end
    B(i) = Soma;
end

F = A\B;

for i = 1: TAM
    Result(i) = 0;
    for j = 1 : GRAU
        Result(i) = Result(i) + F(j)*Mat(j,i) 
    end
end
    
    
//invertendo a matriz Mat para ficar no mesmo sentido do vetor Result
for i = 1 : GRAU
    for j = 1: TAM
        MatInv(j,i) = Mat(i,j);
    end
end

//Imprimindo Resultados no Arquivo 
 
printf("\nResultados da Aproximação Polinomial \n\n");

printf("\nMatriz Mat: [ ");
for i=1:TAM
    for j = 1: GRAU
        printf(" %f ", MatInv(i,j));
    end
        printf(" ,\n\t      ");
end
printf(" ]\n\n");


printf("\nMatriz A: [ ");

for i = 1:GRAU
    for j = 1: GRAU
       printf(" %f ", A(i,j));
    end
        printf(" ,\n\t    ");
end
printf(" ]\n\n" );

printf("\nPontos de B: [ ");

for i = 1:GRAU
    printf("%f ", B(i));
    if(i < GRAU)
        printf(" , ");
    end
end
printf(" ]\n\n" );

printf("\nPontos de F: [ ");
for i = 1:GRAU
    printf("%f ", F(i));

    if(i < GRAU)
        printf(" , ");
    end
end
printf(" ]\n\n" );

printf("\nPontos de Y novo: [ ");

for i = 1:TAM
    printf("%f ", Result(i));

    if(i < TAM)
        printf(" , ");
    end
end
printf(" ]\n\n" );

// Exibe o gráfico da nova função
plot(X,Y,'*green', X,Result,'-red')
