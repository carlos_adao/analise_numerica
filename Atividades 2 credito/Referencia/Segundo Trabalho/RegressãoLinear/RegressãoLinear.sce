
//                       Regressão Linear MMQ                              //

//Aluno: Albert Paixão Santana

//ENTRADA
clc();
X = [1,2,3,4,5,6,7];
Y = [0.5,2.5,2.0,4.0,3.5,6.0,5.5];

TAM = length(X);

//ALGORITMO

SomX = 0;
SomY = 0;
SomXY =0;
SomX2 = 0;
SomY2 = 0;

for cont = 1 : TAM
    SomX = SomX + X(cont);
    SomY = SomY + Y(cont);
    SomXY = SomXY + X(cont)*Y(cont);
    SomX2  = SomX2 + X(cont)*X(cont);
    SomY2 = SomY2 + Y(cont) * Y(cont);
end


Xmed = SomX/TAM;

Ymed = SomY/TAM;

A1 = ( TAM*SomXY - SomX*SomY ) / ( TAM * SomX2 - SomX*SomX );

A0 = Ymed - A1*Xmed;

for i = 1 : TAM
    Ynovo(i) = A0 + A1*X(i);
end

printf("Resultados da Regressão Linear MMQ\n\n")

printf("Somatorio X : %f \n", SomX);
printf("Somatorio X : %f \n", SomY);
printf("Somatorio X : %f \n", SomXY);
printf("Somatorio X : %f \n", SomX2);
printf("Somatorio X : %f \n", SomY2);
printf("Função : Y = %f + %fX \n\n", A0,A1)


printf("Novos Pontos de Y:[ ")
for i = 1:TAM

    printf("%f", Ynovo(i));
    if(i < TAM)

        printf(" , ");
    end
end
printf(" ] ");

// Exibe o gráfico com os novos pontos
plot(X,Y,'*green', X,Ynovo,'red');

