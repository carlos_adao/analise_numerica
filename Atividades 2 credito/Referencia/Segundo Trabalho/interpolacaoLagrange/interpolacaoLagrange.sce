
//                       Interpolação de Newton                            //

//Aluno: Albert Paixão Santana

//ENTRADA

clc

tam = 4

X =[0, 1, 3, 4];
Y =[2, 4, 5, 0];
printf("Matriz X");
disp(X);
printf("\nMatriz Y");
disp(Y);

z = 2

//ALGORITMO

r = 0;

for (i = 1:tam)
    a = 1;
    b = 1;
    for(j = 1:tam)
        if(i <> j)
            a = a * (z - X(j));
            b = b * (X(i) - X(j));
        end
    end
    r = r + Y(i) * a / b;
end

printf("\nResultado da Interpolação: %f", r);











