
//                       Interpolação de Newton                            //

//Aluno: Albert Paixão Santana

//ENTRADA
clc

tam = 5

X = [0, 0.5, 1, 1.5, 2];
Y = [0.0, 1.1487, 2.7183, 4.9811, 8.3890];

printf("Matriz X");
disp(X);
printf("\nMatriz Y");
disp(Y);

x1 = 0.7


//ALGORITMO

cont = 1;

for(i = 1:tam)
    z(cont) = Y(i);   //z recebe os valores de y
    cont = cont + 1; 
end;

for(i = 1:tam-1)
    for(j = 1:tam-i)
        a = cont-tam+i;
        b = cont-tam+i-1;
        z(cont) = (z(a) - z(b))/(X(j+i) - X(j));
        cont = cont + 1;
    end;
end;

cont = 1;

for(i = 1:tam)
    b(i) = z(cont);
    cont = cont + tam - i + 1;
end;

p = 1;
y1 = b(1);

for(i = 2:tam)
    p = (x1 - X(i-1)) * p;
    y1 = y1 + p * b(i);
end;

printf("\nResultado da interpolação de Newton: \n%f", y1);
