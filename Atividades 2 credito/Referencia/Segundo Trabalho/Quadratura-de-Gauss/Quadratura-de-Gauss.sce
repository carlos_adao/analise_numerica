//                       Quadratura de Gauss

//Aluno: Albert Paixão Santana

clc();
//ENTRADA
deff('y=f(x)', 'y = x/(4+x)*%e^((-2*x)/10)');

a = 0
b = 10

//ALGORITMO

a0 = (b+a) / 2;
a1 = (b-a) / 2;

    quad = integrate('f(x)','x',-1,1);

disp('O resultado é:')
disp(abs(quad))
