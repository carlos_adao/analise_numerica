//                  Extrapolação de Richards                               //

//Aluno: Albert Paixão Santana

//ENTRADA
clc

deff('y = f(x)','y = 0.2 + 25 * x - 200 * x^2 + 675 * x^3 - 900 * x^4 + 400 * x^5');

a = 0
b = 0.8

//ALGORITMO

NSub = [1,2,4];
I = [];

for(i = 1:3)
    printf("M = %d\n", NSub(i));
    
    h = (b-a) / NSub(i);
    printf("h = %f\n", h);
    
    x = a + h;
    soma = 0;
    
    for(j = 1:NSub(i)-1)
        soma = soma + 2*f(x);
        x = x + h;
    end;
    
    I(i) = (h/2)*(f(a) + f(b) + soma);
    printf("Integral = %f\n", I(i));
end;

// Escreve no arquivo o resultado
Integral_1 = 4/3 * I(2) - 1/3 * I(1);
printf("\n2-1: %f", Integral_1);

Integral_2 = 4/3 * I(3) - 1/3 * I(2);
printf("\n4-2: %f", Integral_2);

Integral_3 = 16/15 * Integral_2 - Integral_1 * 1/15 ;
printf("\n%f", Integral_3); 

