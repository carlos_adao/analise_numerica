//                       Regra de Sipson 1/3                               //

//Aluno: Albert Paixão Santana

//ENTRADA
clc 

//deff('y = f(x)','y = 1 /(x^2)'); //intervalo a=1, b=3, NSub=4
deff('y=f(x)', 'y = x/(4+x)*%e^((-2*x)/10)');

a = 0;
b = 10;
NSub = 2; //Quantidade de subintervalos

//ALGORITMO

// Testa se o valor do Subintervalos é válido
if(NSub == 0) then
    printf("Erro: Divisao por zero");
    return
end;

if(NSub < 0) then
    printf("Erro: Intervalo invalido");
    return
end;

if(modulo(NSub,2) ~= 0) then
    printf("Erro: Intervalo nao par");
    return
end;

h = (b-a)/NSub;
x = a + h;

somaP = 0;
somaI = 0;

for(i = 1:NSub-1)
    
    if(modulo(i,2) == 0) then
        somaP = somaP + f(x);
    else
        somaI = somaI +f(x);
    end;
    x = x + h;    
end;

I = (h/3)*(f(a) + f(b) + 4*somaP +2*somaI);

printf('\nResultado da Integracao: %f\n\n\n', I);

