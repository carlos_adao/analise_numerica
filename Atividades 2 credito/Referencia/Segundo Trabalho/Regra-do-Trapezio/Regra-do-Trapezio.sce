
//                       Regra do Trapézio                                 //

//Aluno: Albert Paixão Santana

//ENTRADA

clc();
//deff('y=f(x)', 'y = (%e^x) * cos(x)'); //intervalo a=0, b=1.2 e NSub = 6
deff('y=f(x)', 'y = x/(4+x)*%e^((-2*x)/10)');

a = 0;
b = 10;
NSub = 1;//Quantidade de subintervalos

//ALGORITMO

if(NSub == 0) then
    printf("\nErro: Divisão por zero");
    return
end;

// Se o intervalo é negativo, retorna uma mensagem de erro
if(NSub < 0) then
    printf("\nErro: Intervalo inválido!\n")
    return
end;

h = (b-a)/NSub;
x = a + h;
soma = 0;

for(i = 1:NSub-1)  
    soma = soma + 2*f(x);
    x = x + h;
end;

I = (h/2)*(f(a) + f(b) + soma);

printf("\nResultado da integração: \n%f", I);

